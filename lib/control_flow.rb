# EASY

# Return the argument with all its lowercase characters removed.

#create new string
#iterate through string
#check if current letter is uppercase
  #if so, add to new string
#return the new string
def destructive_uppercase(str)
  uppercased_letters = ''
  str.each_char do |ch|
    next if ch.upcase != ch
    uppercased_letters += ch
  end
  uppercased_letters
end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"

#change string into array
#if length of array is odd
#divide the length-1 by 2 save into variable
#if length of array is even
#save second variable which is the first variable plus 1
#return array elements at the location of variables
def middle_substring(str)
  str_array = str.split('')
    if str_array.length.odd?
      sub_odd = (str_array.length-1) / 2
      return str_array[sub_odd]
    else
      sub_odd = (str_array.length-1) / 2
      sub_even = sub_odd + 1
      return str_array[sub_odd] + str_array[sub_even]
    end
end

# Return the number of vowels in a string.

#create a counter
#iterate through the string and check if it includes any letters from the vowels array
  #if so, add 1 to the counter
#return the counter
VOWELS = %w(a e i o u) #this is now an array
def num_vowels(str)
  counter = 0
    str.each_char do |ch|
      if VOWELS.include?(ch)
        counter += 1
      end
    end
  counter
end

# Return the factorial of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.

#create a variable which will house the product, set equal to 1
#iterate down from the num
  #multiply variable by current num
#return variable
def factorial(num)
  product = 1
    while num > 0
      product *= num
      num -= 1
    end
  product
end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.

#create a new variable which will house the string
#iterate through the array
#if current iteration is the last one do not add the separator, only add the item
#add each item and the separator to the new variable
#return the new variable
def my_join(arr, separator = "")
  new_str = ''
    arr.each_with_index do |el, idx|
      if idx == arr.length-1
        new_str += el
      else
        new_str += el
        new_str += separator
      end
    end
  new_str
end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"

#create a new variable to house new string
#iterate through string
#if index is even change to lowercase and save to new string
#if index is odd change to uppercase and save to new string
#return new string
def weirdcase(str)
  str_array = str.split('')
  weird_str = ''
    str_array.each_with_index do |el, idx|
      if idx.even?
        weird_str += el.downcase
      else
        weird_str += el.upcase
      end
    end
  weird_str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")

#reverse_five('well hello mother') => 'well olleh rehtom'
#*itereate through the string and count how many letters are in each word, reverse when more than five*

#change string into an array of words
#iterate through array
#count the length of each word
  #if word count five or greater reverse the word
  #replace word with reversed word in given array
#return array as a string
def reverse_five(str)
  str_array = str.split(' ')
  str_array.each_with_index do |el, idx|
    if el.length > 4
      str_array[idx] = el.reverse
    end
  end
  str_array.join(' ')
end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".

#fizzbuzz(15) => 1 2 fizz 4 buzz fizz 7 8 fizz buzz 11 fizz 13 14 fizzbuzz
#*start at 1 and count up to and include num input, each number divisible by three replace with 3 and each
#num divisible by 5 replace with buzz and each num divisible by 3 and 5 replace with fizzbuzz*

#create array variable
#iterate down from given num
#if num is divisible by 5 and 3 unshift fizzbuzz into array
#if num is divisible by 5 unshift buzz into array
#if num is divisible by 3 unshift fizz into array
#finally unshift num into array
#return the final array
def fizzbuzz(n)
  fb_array = []
  while n > 0
    if n % 15 == 0
      fb_array.unshift('fizzbuzz')
      n -= 1
    elsif n % 5 == 0
      fb_array.unshift('buzz')
      n -= 1
    elsif n % 3 == 0
      fb_array.unshift('fizz')
      n -= 1
    else
      fb_array.unshift(n)
      n -= 1
    end
  end
  fb_array
end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.

#my_reverse([1,2,3]) => [3,2,1]
#*iterates from the end of the array first*

#create new array
#iterate through array
#save to the front of the new array
#return the reversed array
def my_reverse(arr)
  rev_array = []
  arr.each do |el|
    rev_array.unshift(el)
  end
  rev_array
end

# Write a method that returns a boolean indicating whether the argument is
# prime.

#prime?(7) => true
#prime?(8) => false
#prime?(182) => false
#*if a number is only divisible by itself and 1 it is prime*

#iterate down from given num
  #check if num is evenly divisible by anything below itself
  #if it is return false
#if not return true
def prime?(num)
    if num == 1
      return false
    end
  start_num = num - 1
  while start_num > 1
    if num % start_num == 0
      return false
    else
      start_num -= 1
    end
  end
  return true
end

# Write a method that returns a sorted array of the factors of its argument.

#factors(10) => [1,2,5,10]
#factors(12) = [1,2,3,4,6,12]
#*iterate down from given num and check if decrementing num is divisible*

#create new array variable
#create counter variable that starts at 1
#loop up to num
  #check if current num divides evenly into give num
  #if so save to array
  #increment
#increment if not
#return given array
def factors(num)
  factors_arr = []
  counter = 1
  while counter <= num
    if num % counter == 0
      factors_arr << counter
      counter += 1
    else
      counter += 1
    end
  end
  factors_arr
end

# Write a method that returns a sorted array of the prime factors of its argument.
#prime_factors(10) => [2,5]
#prime_factors(12) => [2,3]
#*find factors and check if each number is also prime*

#create new array variable
#create counter variable that starts at 2
#loop up to num
  #check if current num divides evenly into given num and is prime
  #if so save to array
  #increment
#increment if not
#return teh final array
def prime_factors(num)
  pf_array = []
  counter = 2
  while counter <= num
    if num % counter == 0 && prime?(counter)
      pf_array << counter
      counter += 1
    else
      counter += 1
    end
  end
  pf_array
end

# Write a method that returns the number of prime factors of its argument.
#*find the length of prime_factors for given number*
def num_prime_factors(num)
  prime_factors(num).length
end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity,
#e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
#*separate array elements by evens and odds, whichever array only has one item should have that item returned*

#create an odd array
#create an even array
#iterate through the array
  #if current element is odd
  #save to odd array
  #if current element is even
  #save to even array
#return the item that is singular
def oddball(arr)
  odd_array = []
  even_array = []
    arr.each do |el|
      if el.odd?
        odd_array << el
      else
        even_array << el
      end
    end
  if odd_array.length == 1
    return odd_array[0]
  else
    return even_array[0]
  end 
end
